function makeInfoWindow(feature, layer) {
   return layer.get('dlname') + ': ' + feature.get('country') + ', ' + feature.get('station') + ', <b>Elevation:</b> ' + feature.get('elevation');
   //ol.coordinate.toStringXY(ol.proj.toLonLat(evt.coordinate,map.getView().getProjection()),3);
}

function makeStyle(color, text) {
   var style = new ol.style.Style({
      image: new ol.style.Circle({ radius: 7, stroke: new ol.style.Stroke({ color: [0,0,0,0.5], width: 1, }), fill: new ol.style.Fill({ color: color, }), }),
      //text: new ol.style.Text({ text: text, fill: new ol.style.Fill({ color: '#fff' }), textAlign: 'left', offsetX: 7, scale: 1, padding: [1,1,1,1], backgroundFill: new ol.style.Fill({ color: [255,255,255,0.3] }), }),
    });
    return style;
}
var redStyle = makeStyle([255,0,0,1],'');
var yellowStyle = makeStyle([255,255,0,1],'');
var greenStyle = makeStyle([0,255,0,1],'');

var stroke = new ol.style.Stroke({color: 'black', width: 1});
var fill = new ol.style.Fill({color: 'blue'});

var squareStyle = new ol.style.Style({
   image: new ol.style.RegularShape({ fill: fill, stroke: stroke, points: 4, radius: 7, angle: Math.PI / 4 })
});

var styles = {
   1:  new ol.style.Style({ stroke: new ol.style.Stroke({ color: 'rgba(0, 0, 0, 1)', width: 0.1 }), fill: new ol.style.Fill({ color: 'rgba(220, 240, 220, 1)' }), }),
   2:  new ol.style.Style({ stroke: new ol.style.Stroke({ color: 'rgba(0, 0, 0, 1)', width: 0.1 }), fill: new ol.style.Fill({ color: 'rgba(250, 230,  30, 1)' }), }),
   3:  new ol.style.Style({ stroke: new ol.style.Stroke({ color: 'rgba(0, 0, 0, 1)', width: 0.1 }), fill: new ol.style.Fill({ color: 'rgba(230, 120,   0, 1)' }), }),
   88: new ol.style.Style({ stroke: new ol.style.Stroke({ color: 'rgba(0, 0, 0, 1)', width: 0.1 }), fill: new ol.style.Fill({ color: 'rgba(200,   0,   0, 1)' }), }),
   99: new ol.style.Style({ stroke: new ol.style.Stroke({ color: 'rgba(0, 0, 0, 1)', width: 0.1 }), fill: new ol.style.Fill({ color: 'rgba(100,   0,   0, 1)' }), }),
};
var styleFunction = function(feature) {
   return styles[feature.getProperties()['CS']];
};


var vectorSource1 = new ol.source.Vector({ url: 'https://iridl.ldeo.columbia.edu/dlcharts/render/d0379821527e1fda87a9221bbf5afee97e8b2fea', format: new ol.format.GeoJSON() });
var vectorSource2 = new ol.source.Vector({ url: 'https://iridl.ldeo.columbia.edu/dlcharts/render/88acd6f3edbfa119d4bbb5e8f2fa9bcf73ed8c7b', format: new ol.format.GeoJSON() });
var clusterSource = new ol.source.Cluster({ distance: 30, source: vectorSource1 });

var styleCache = {};
var clusterLayer = new ol.layer.Vector({
   source: clusterSource,
   style: function(feature) {
     var size = feature.get('features').length;
     if (size==1) {
        var props = feature.getProperties();
        style = (props['elevation']>500 ? redStyle : yellowStyle);
     } else {
        var style = styleCache[size];
        if (!style) {
          style = new ol.style.Style({
            image: new ol.style.Circle({ radius: 15, stroke: new ol.style.Stroke({ color: '#fff' }), fill: new ol.style.Fill({ color: '#3399CC' }), }),
            text: new ol.style.Text({ text: size.toString(), fill: new ol.style.Fill({ color: '#fff' }), }),
          });
          styleCache[size] = style;
        }
     }
     return style;
   }
});

var vectorLayer = new ol.layer.Vector({ source: vectorSource2, style: function (feature, resolution) { return [squareStyle]; } });

createGMapDLImage({
     id: 'map1',
     debug: false,
     mapOptions: {
     },
     viewOptions: {
        center: [30,5],
        zoom: 3, 
        rotation: 0,
        projection: 'EPSG:4326', // 'EPSG:4326' | 'EPSG:3857'
     },

     layers: [
        { type: 'ol', name: 'OSM', opacity: 1.0, showOpacityControl: false, layer: new ol.layer.Tile({ source: new ol.source.OSM({wrapX: true}) }) },
        { type: 'ol', name: 'MapBox', opacity: 0.2, showOpacityControl: true, layer: new ol.layer.Tile({ minResolution: 0.015, source: new ol.source.TileJSON({ url: 'https://api.tiles.mapbox.com/v3/mapbox.natural-earth-hypso-bathy.json?access_token=pk.eyJ1IjoiaWtoIiwiYSI6ImNqcWNpMW12ajF3MGk0M3FxMXoxcm41NHUifQ.ymu5eFfcyRgc6wOs2zHLrw', crossOrigin: 'anonymous' }) }) },
        //{ type: 'iridl', name: 'Elevation', opacity: 0.0, url: 'https://iridl.ldeo.columbia.edu/SOURCES/.WORLDBATH432/.bath/X/Y/fig-+colors+-fig'} ,
        //{ type: 'iridl', name: 'WorldClim', opacity: 0.0, url: 'http://iridl.ldeo.columbia.edu/SOURCES/.WORLDCLIM/.alt/a-/-a/X+Y+fig-+colors+coasts+lakes+-fig'} ,
        { type: 'iridl', name: 'Elevation', opacity: 0.0, url: 'https://iridl.ldeo.columbia.edu/SOURCES/.CIAT/.CSI/.SRTM/.version4p1/.SRTM/a-/-a/X+Y+fig-+colors+coasts+lakes+-fig'} ,
        //{ type: 'iridl', name: 'IRI Data Library', opacity: 0.5, url: 'https://iridl.ldeo.columbia.edu/expert/%28koeppen%29//maptype/parameter/%28koeppen%29eq/%7BSOURCES/.FAO/.NRMED/.SD/.Climate/.Derived/.class%7Dif/%28koeppen%29//maptype/parameter/%28prec%29eq/%7BSOURCES/.UEA/.CRU/.TS2p1/.climatology/.c7100/.prcp/DATA/0/700/RANGE%7Dif/%28koeppen%29//maptype/parameter/%28wetdays%29eq/%7BSOURCES/.UEA/.CRU/.TS2p1/.climatology/.c7100/.wet%7Dif/%28koeppen%29//maptype/parameter/%28meantemp%29eq/%7BSOURCES/.UEA/.CRU/.TS2p1/.climatology/.c7100/.mean/.temp/DATA/-55/45/RANGE%7Dif/%28koeppen%29//maptype/parameter/%28mintemp%29eq/%7BSOURCES/.UEA/.CRU/.TS2p1/.climatology/.c7100/.minimum/.temp/DATA/-55/45/RANGE%7Dif/%28koeppen%29//maptype/parameter/%28maxtemp%29eq/%7BSOURCES/.UEA/.CRU/.TS2p1/.climatology/.c7100/.maximum/.temp/DATA/-55/45/RANGE%7Dif/%28koeppen%29//maptype/parameter/%28frostdays%29eq/%7BSOURCES/.UEA/.CRU/.TS2p1/.climatology/.c7100/.frost%7Dif/%28koeppen%29//maptype/parameter/%28elev%29eq/%7BSOURCES/.NOAA/.NGDC/.ETOPO1/.z_ice/lon/%28X%29renameGRID/lat/%28Y%29renameGRID/X/-180/180/RANGE/Y/-90/90/RANGE/startcolormap/DATA/0/8752/RANGE/white/white/LightGoldenrodYellow/0/VALUE/LightGoldenrod/500/VALUE/goldenrod/1500/VALUE/DarkGoldenrod/3000/VALUE/sienna/8224/VALUE/sienna/endcolormap%7Dif/X/Y/fig-/colors black countries coasts lakes/-fig//T/last/plotvalue'} ,
        //{ type: 'iridl', name: 'CHIRPS', opacity: 0.2, url: 'https://iridl.ldeo.columbia.edu/SOURCES/.UCSB/.CHIRPS/.v2p0/.monthly/.global/.precipitation/X/Y/fig-+colors+-fig//T/last/plotvalue'} ,
        //{ type: 'iridl', name: 'CMAP Climo', opacity: 0.5, url: 'https://iridl.ldeo.columbia.edu/SOURCES/.IRI/.MD/.IFRC/.CPC/.Merged_Analysis/.monthly/.v1201/.prcp_est/X/Y/fig-+colors+coasts+-fig//T/(Jan)/plotvalue'},
        //{ type: 'ol', name: 'NOAA Stations', opacity: 1.0, showOpacityControl: true, layer: clusterLayer },
        //{ type: 'ol', name: 'Random Points', opacity: 0.0, showOpacityControl: true, layer: vectorLayer },
        { type: 'ol', name: 'IPC_2008', opacity: 0.5, showOpacityControl: true, layer: new ol.layer.Vector({ source: new ol.source.Vector({ format: new ol.format.GeoJSON(), url: 'https://iridl.ldeo.columbia.edu/dlcharts/render/0d2859f9e7919548598fd1e897e77a8445f3e333?year=2008' }), style: styleFunction, }), },
        { type: 'ol', name: 'IPC_2009', opacity: 0.5, showOpacityControl: true, layer: new ol.layer.Vector({ source: new ol.source.Vector({ format: new ol.format.GeoJSON(), url: 'https://iridl.ldeo.columbia.edu/dlcharts/render/0d2859f9e7919548598fd1e897e77a8445f3e333?year=2009' }), style: styleFunction, }), },
        { type: 'ol', name: 'IPC_2010', opacity: 0.5, showOpacityControl: true, layer: new ol.layer.Vector({ source: new ol.source.Vector({ format: new ol.format.GeoJSON(), url: 'https://iridl.ldeo.columbia.edu/dlcharts/render/0d2859f9e7919548598fd1e897e77a8445f3e333?year=2010' }), style: styleFunction, }), },
        { type: 'ol', name: 'IPC_2011', opacity: 0.5, showOpacityControl: true, layer: new ol.layer.Vector({ source: new ol.source.Vector({ format: new ol.format.GeoJSON(), url: 'https://iridl.ldeo.columbia.edu/dlcharts/render/0d2859f9e7919548598fd1e897e77a8445f3e333?year=2011' }), style: styleFunction, }), },
        { type: 'ol', name: 'IPC_2012', opacity: 0.5, showOpacityControl: true, layer: new ol.layer.Vector({ source: new ol.source.Vector({ format: new ol.format.GeoJSON(), url: 'https://iridl.ldeo.columbia.edu/dlcharts/render/0d2859f9e7919548598fd1e897e77a8445f3e333?year=2012' }), style: styleFunction, }), },
        { type: 'ol', name: 'IPC_2013', opacity: 0.5, showOpacityControl: true, layer: new ol.layer.Vector({ source: new ol.source.Vector({ format: new ol.format.GeoJSON(), url: 'https://iridl.ldeo.columbia.edu/dlcharts/render/0d2859f9e7919548598fd1e897e77a8445f3e333?year=2013' }), style: styleFunction, }), },
        { type: 'ol', name: 'IPC_2014', opacity: 0.5, showOpacityControl: true, layer: new ol.layer.Vector({ source: new ol.source.Vector({ format: new ol.format.GeoJSON(), url: 'https://iridl.ldeo.columbia.edu/dlcharts/render/0d2859f9e7919548598fd1e897e77a8445f3e333?year=2014' }), style: styleFunction, }), },
        { type: 'ol', name: 'IPC_2015', opacity: 0.5, showOpacityControl: true, layer: new ol.layer.Vector({ source: new ol.source.Vector({ format: new ol.format.GeoJSON(), url: 'https://iridl.ldeo.columbia.edu/dlcharts/render/0d2859f9e7919548598fd1e897e77a8445f3e333?year=2015' }), style: styleFunction, }), },
        { type: 'ol', name: 'IPC_2016', opacity: 0.5, showOpacityControl: true, layer: new ol.layer.Vector({ source: new ol.source.Vector({ format: new ol.format.GeoJSON(), url: 'https://iridl.ldeo.columbia.edu/dlcharts/render/0d2859f9e7919548598fd1e897e77a8445f3e333?year=2017' }), style: styleFunction, }), },
        { type: 'ol', name: 'IPC_2017', opacity: 0.5, showOpacityControl: true, layer: new ol.layer.Vector({ source: new ol.source.Vector({ format: new ol.format.GeoJSON(), url: 'https://iridl.ldeo.columbia.edu/dlcharts/render/0d2859f9e7919548598fd1e897e77a8445f3e333?year=2017' }), style: styleFunction, }), },
     ],

     mapClick: { 
        type: 'rectangle', // none | click | marker | rectangle
        showFeature: true,
        moveOnClick: true,
        setBounds: true,
        marker: {
           draggable: false,
           position: [30,5],
        },
        rectangle: {
           bounds: [27,2,33,8],
           draggable: true,
           editable: false,
        } 
     },

     featureClick: {
         type: 'infoWindow', //none | click | infoWindow
         makeInfoWindow: makeInfoWindow, //function
     },

});
